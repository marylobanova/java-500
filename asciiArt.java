public class asciiArt 
{   
   //each tile's width
   public static int w = 4;
   
   //each tile's height
   public static int h = 5;
   
   //number of full tiles across
   public static int i = 2;
   
   //number of full tiles down
   public static int j = 1;
   
   public static void main(String[] args) 
   {
      addTileHeightOdd();
      printStraightLine();
      printPattern();
      printStraightLine();
   }

   //prints as many rows and columns of tiles as it is required
   public static void printPattern() 
   {
      for(int height = j+1; height > 0; height--) {
         addTileHeight();
         printBackslash();
         printTileMiddle();
         printBackslash();
         addTileHeight();
         addTileHeightOdd();
      }
   }
      
   //prints vertical line with left and right spacing equal to tile width      
   public static void printStraightLine() 
   {
      System.out.print("|");
      for(int evenLength = i+1; evenLength > 0; evenLength--) {
         printSpace(); 
         System.out.print("|");
         printSpace();      
      for(int oddLength = w%2; oddLength > 0; oddLength--)
         System.out.print(" ");
      }
      System.out.println();
   }
   
   //prints backslash with left and right spacing equal to tile width 
   public static void printBackslash() 
   {
      System.out.print("|");
      for(int evenLength = i+1; evenLength > 0; evenLength--) {
         printSpace();
         System.out.print("\\");
         printSpace();
      for(int oddLength = w%2; oddLength > 0; oddLength--)
         System.out.print(" ");
      }
      System.out.println();
   }
   
   //prints middle part of the tile according to tile width
   public static void printTileMiddle() 
   {
      System.out.print("|");
      for(int evenLength = i+1; evenLength > 0; evenLength--) {
         printDash();
         System.out.print("\\ \\");
         printDash();  
      for(int oddLength = w%2; oddLength > 0; oddLength--)
         System.out.print("-"); 
      }           
      System.out.println();
   }  
   
   //prints as many vertical lines as it is required by tile height  
   public static void addTileHeight() 
   {
      for(int tileHeight = h-3; tileHeight > 0; tileHeight--)
         printStraightLine();
   }
   
   //prints dashes for 1/2 of total tile width
   public static void printDash() 
   {
      for(int tileWidth = w/2; tileWidth > 0; tileWidth--) 
         System.out.print("-");
   }
   
   //prints spaces for 1/2 of total tile width
   public static void printSpace() 
   {
      for(int tileWidth = w/2+1; tileWidth > 0; tileWidth--) 
         System.out.print(" ");
   }     
   
   //prints additional line if height is odd 
   public static void addTileHeightOdd()
   {
      for(int oddHeight = h%2; oddHeight > 0; oddHeight--)
         printStraightLine();
   }         
}
