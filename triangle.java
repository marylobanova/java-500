import java.util.*;
public class triangle 
{
   public static void main(String[] args)
   {
      double a, b, c, area, perimeter;
      Scanner in = new Scanner(System.in);
      
      //get lengths of sides
      a = input(in, "Enter side 1: ");
      b = input(in, "Enter side 2: ");
      c = input(in, "Enter side 3: ");
      
      //check for zero and negative numbers
      while (a<=0 || b<=0 || c<=0) {
         System.out.println("Length of sides can not be less or equal to zero. Please try again");
         System.out.println();
         a = input(in, "Enter side 1: ");
         b = input(in, "Enter side 2: ");
         c = input(in, "Enter side 3: ");
      }
      
      //check if three values can make up a triangle    
      if (a < (b + c) &&
          b < (a + c) &&
          c < (a + b)) {
          
         //check if all sides are different
         if (a!=b && b!=c && c!=a){
            System.out.println("No equal sides - this triangle is scalene");
            if ((a*a == b*b + c*c) ||
                (b*b == a*a + c*c) ||
                (c*c == a*a + b*b))
                System.out.println("And it is a right triangle");
         }           
         //check if all sides are equal
         else if (a==b && b==c && c==a) 
            System.out.println("All sides equal - this triangle is equilateral");
         
         //check of any two sides are equal
         else if (a==b || b==c || c==a)
            System.out.println("Two sides equal - this triangle is isosceles");
            
            //get area      
            area = Math.round(area(a, b, c));
            //get perimeter
            perimeter = perimeter(a, b, c);
            System.out.println("Triangle area (rounded) = " + area);
            System.out.println("Triangle perimeter = " + perimeter);                   
      }

      else 
         System.out.println("Not sides of a triangle");       
   }

   public static double input(Scanner in, String s)
   {
      System.out.print(s);
      return in.nextDouble();
   }
   
   //calculate area
   public static double area (double a, double b, double c) 
   {
      double s = (a + b + c)/2;
      double area = Math.sqrt(s*(s - a)*(s - b)*(s - c));
      return area;
   }
   
   //calculate perimeter
   public static double perimeter (double a, double b, double c)
   {
      double perimeter = (a + b + c);
      return perimeter;
   }
}
