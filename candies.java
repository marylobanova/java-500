/** You are provided with initial number of candies and with number of wrappers 
that you can exchange to get an additional candy. Program should calculate the total 
number of candies that you can have after all exchanges. **/

import java.util.*;
public class candies 
{
   public static int candies,
                     count;
   
   public static void main(String[] args) 
   {
      Scanner in = new Scanner(System.in); 
      candies = input(in, "How many candies do you have? ");
      count = input(in, "How many wrappers you can exchange for a new candy? ");
      calculate();
   }      
   
   public static void calculate() 
   {
      int totalWrap = candies;
      int newWrap, remWrap;
      
      do {
         newWrap = totalWrap/count;
         remWrap = totalWrap%count; 
         totalWrap = newWrap + remWrap;
         candies += newWrap;
         }
         while (newWrap > 0);
         
      System.out.print("Total number of candies you can get is " + candies);
   }
      
   public static int input(Scanner in, String s)
   {
      System.out.print(s);
      return in.nextInt();
   }
   
}

         
 