import java.util.*;
public class ISP
{
   public static void main(String[] args)
   {
      int servicePlan, month, correctMonth, year;
      double correctHours, hours, cost1, cost2, cost3;
      Scanner in = new Scanner(System.in); 
      
      //get month and check for validity
      month = inputInt(in, "Enter bill month (1-12): ");
      while (month < 1 || month > 12) {     
         System.out.println ("Invalid entry. Please enter the number between 1 and 12");
         month = inputInt(in, "Enter bill month (1-12): ");
      }
      
      //get year and check for validity
      year = inputInt(in, "Enter billing year: ");
      while (year < 1) {     
         System.out.println ("Invalid entry. Please enter a positive number");
         year = inputInt(in, "Enter billing year: ");
      }
         
      //get service plan and check for validity      
      servicePlan = inputInt(in, "Which service package did you purchase? Please select one of the following: \n * For $9.95/10 hours enter \"1\"\n * For $14.95/20 hours enter \"2\"\n * For $19.95/unlimited hours enter \"3\"\n");  
      while (servicePlan < 1 || servicePlan > 3) {     
         System.out.println ("Invalid entry. Please enter the number between 1 and 3");
         servicePlan = inputInt(in, "Which service package did you purchase? Please select one of the following: \n * For $9.95/10 hours enter \"1\"\n * For $14.95/20 hours enter \"2\"\n * For $19.95/unlimited hours enter \"3\"\n");
      }
      
      //get hours and check for validity
      hours = inputDbl(in, "How many hours did you use during this month? ");
      correctHours = checkHours(hours, month, year);
      while ((hours <= 0) || (correctHours == 0)) {     
         System.out.println ("Invalid entry. Please enter a valid number");
         hours = inputDbl(in, "How many hours did you use during this month? ");
         correctHours = checkHours(hours, month, year);
      }
         
      //get total cost for plan 1
      cost1 = calculateCostForPlan1(hours);
      //get total cost for plan 2
      cost2 = calculateCostForPlan2(hours);
      cost3 = 19.95;     
      
      //check savings 
      //if user subscribed to plan 1
      if (servicePlan == 1) {
         System.out.println("Your cost is $" + cost1);
         if  (cost1 > cost2) {
            System.out.println("You would have saved $" + (cost1 - cost2) + " with service plan 2");
            System.out.println("You would have saved $" + (cost1 - cost3) + " with service plan 3");
         }
      }
         //if user subscribed to plan 2
         else if (servicePlan == 2) {
         System.out.println("Your cost is $" + cost2);
         if (cost2 > cost3)
            System.out.println("You would have saved $" + (cost2 - cost3) + " with service plan 3");
         }
         
         //if user subscribed to plan 3
         else if (servicePlan == 3)
         System.out.println("Your cost is $" + cost3);
   }
      
   //calculate cost for plan 1 based on the number of hours
   public static double calculateCostForPlan1 (double hours)
   {
      double fixedCost = 9.95;
      double additionalCost = (hours - 10)*2;
      double totalCost = fixedCost + additionalCost;
      if (hours <= 10)
         return fixedCost;
         else
         return totalCost;
   }
   
   //calculate cost for plan 2 based on the number of hours
   public static double calculateCostForPlan2 (double hours)
   {
      double fixedCost = 14.95;
      double additionalCost = (hours - 20)*1;
      double totalCost = fixedCost + additionalCost;
      if (hours <= 20)
         return fixedCost;
         else
         return totalCost;
   }
   
   //check if provided hours are correct for selected month/year
   public static double checkHours (double hours, int month, int year)
   {
      int leap = isLeap(year);
      //if month has 31 days
      if ((month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) && (hours <= 744))
         return 1;
         //if month has 30 days
         else if ((month == 4 || month == 6 || month == 9 || month == 11) && (hours <= 720))
         return 1;
         //if month is February
         else if ((month == 2) && (leap == 0 && hours <= 672) || (leap == 1 && hours <= 696))
         return 1;    
         //if none of the above is correct - hours are incorrect
         else
         return 0;
   }
   
   //determine if year is leap
   public static int isLeap (int year) 
   {
      if ((year%4 == 0 && year%100 != 0) || year%400 == 0)   
         return 1;
         else return 0;
   }
   
   //integer input
   public static int inputInt(Scanner in, String s)
   {
      System.out.print(s);
      return in.nextInt();
   }
   
   //double input
   public static double inputDbl(Scanner in, String s)
   {
      System.out.print(s);
      return in.nextDouble();
   }
}