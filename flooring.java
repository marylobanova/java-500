import java.util.*;
public class flooring 
{
   public static void main(String[] args) 
   {
      double l, w, cost, surface, cartons, hours, laborCost, supplyCost, flooringCost;
      Scanner in = new Scanner(System.in);
      l = input(in,"Enter length of room (ft): ");
      w = input(in,"Enter width of room (ft): ");
      cost = input(in,"Enter cost of flooring ($ per square foot): ");
      surface = calculateSurface (l, w);
      cartons = calculateCartonsNumber (surface);
      hours = calculateLaborHours (surface);
      laborCost = calculateLaborCost (hours);
      supplyCost = calculateSuppliesCost (laborCost);
      flooringCost = calculateFlooringCost (cartons, cost);          
      printSummary (hours, cartons, flooringCost, supplyCost, laborCost);
   }
   
   //print receipt with summary of work and costs
   public static void printSummary (double hours, double cartons, double flooringCost, double supplyCost, double laborCost)
   {
      double totalCost = flooringCost + supplyCost + laborCost;
      System.out.println("Hours of labor required: " + hours);
      System.out.println("Cartoons of flooring needed: " + cartons);
      System.out.println("Cost of the flooring: $" + flooringCost);
      System.out.println("Cost of supplies: $" + supplyCost);
      System.out.println("Labor cost: $" + laborCost);
      System.out.println("---------------------");
      System.out.println("Total cost: $" + totalCost);
   }
   
   public static double input(Scanner in, String s)
   {
      System.out.print(s);
      return in.nextDouble();
   }
   
   //multiply room's length by width
   public static double calculateSurface (double l, double w)
   {
      double surface = l*w;
      return surface;
   }
   
   //divide total surface by defined dimentions of cartons
   public static double calculateCartonsNumber(double surface)
   {
      double cartonNumber;
      cartonNumber = (int)(Math.ceil (surface/12));
      return cartonNumber;
   }      
   
   //calculate hourly rate and round it
   public static double calculateLaborHours(double surface) 
   {
      double workSpeed = Math.round (200/3);
      double laborHours = Math.round (surface/workSpeed);
      return laborHours;
   }
   
   //calculate labor cost for the job
   public static double calculateLaborCost(double hours)
   {
      double laborCost = hours*40;
      return laborCost;
   }
   
   public static double calculateSuppliesCost (double laborCost)
   {
      double supplyCost = laborCost*0.2;
      return supplyCost;
   }
   
   //caclulate flooring cost based on the number of cartons needed
   public static double calculateFlooringCost (double cartons, double cost)
   {  
      double flooringCost = cartons*12*cost;
      return flooringCost;
   }   
}